import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { take } from 'rxjs';
import { AccountService } from '../services/account.service';
import { AccountDto, LoginDto } from '../shared/account';
import jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
})
export class AccountComponent implements OnInit {
  formAccount: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(private accountService: AccountService, private router: Router) {}
  ngOnInit() {}

  public onLogin() {
    let account = {} as LoginDto;
    account.email = 'admin@gmail.com';
    account.password = 'Admin12345';
    this.accountService
      .login(account)
      .pipe(take(1))
      .subscribe((result: AccountDto) => {
        this.decodedAccessToken(result.token);
        localStorage.setItem('token', result.token);
        this.router.navigate(['student']);
      });
  }

  decodedAccessToken(token: string) {
    var result: any = jwt_decode(token);
    localStorage.setItem('role', result.role);
    localStorage.setItem('firstName', result.firstName);
    localStorage.setItem('lastName', result.lastName);
  }
}
