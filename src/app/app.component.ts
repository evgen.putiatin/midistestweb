import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'MidisTestWeb';

  constructor(private router: Router) {
    let path: string =
      localStorage.getItem('token') == null ? '/login' : '/student';
    console.log(localStorage.getItem('token'));
    this.router.navigate([path]);
  }
}
