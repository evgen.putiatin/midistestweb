import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AccountDto, LoginDto } from '../shared/account';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  baseURL: string = 'https://localhost:7189/';

  constructor(private http: HttpClient) {}
  login(request: LoginDto): Observable<AccountDto> {
    return this.http.post<AccountDto>(
      this.baseURL + 'api/account/login',
      request
    );
  }
}
