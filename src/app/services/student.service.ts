import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StudentDto } from '../shared/student';

@Injectable({
  providedIn: 'root',
})
export class StudentService {
  constructor(private http: HttpClient) {}

  baseURL: string = 'https://localhost:7189/';

  list(): Observable<any> {
    return this.http.get<any>(this.baseURL + 'api/student/list');
  }
}
