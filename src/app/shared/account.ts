export interface AccountDto {
  id: string;
  email: string;
  userName: string;
  expireDate: Date;
  token: string;
  successful: boolean;
  errorMessage: string;
}

export interface LoginDto {
  email: string;
  password: string;
}
