export interface StudentDto {
  id: string;
  firstName: string;
  lastName: string;
  birstYear: Date;
  email: string;
  phoneNumber: string;
  address: string;
  city: string;
  trainingCategory: TrainingCategory;
  theory: boolean;
  practicalDriving: boolean;
  examDate?: Date;
}

enum TrainingCategory {
  A,
  A1,
  B,
  BE,
  C,
  CE,
  D,
  M,
}
