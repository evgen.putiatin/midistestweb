import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StudentService } from '../services/student.service';
import { StudentDto } from '../shared/student';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss'],
})
export class StudentComponent implements OnInit {
  gridTableSettings = [
    { title: 'First name', field: 'firstName', type: 'string' },
    { title: 'Last name', field: 'lastName', type: 'string' },
    { title: 'Birth year', field: 'birstYear', type: 'Date' },
    { title: 'Email', field: 'email', type: 'string' },
    { title: 'Phone number', field: 'phoneNumber', type: 'string' },
    { title: 'Address', field: 'address', type: 'string' },
    { title: 'City', field: 'city', type: 'string' },
    { title: 'Training category', field: 'trainingategory', type: 'enum' },
    { title: 'Theory', field: 'theory', type: 'boolean' },
    { title: 'Practical driving', field: 'practicalDriving', type: 'boolean' },
    { title: 'Exam date', field: 'examDate', type: 'Date' },
  ];

  studentItems: StudentDto[];
  constructor(private studentService: StudentService, private router: Router) {}

  ngOnInit() {
    this.studentService.list().subscribe((result: StudentDto[]) => {
      this.studentItems = result;
      console.log(this.studentItems);
    });
  }

  onLogout() {
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('firstName');
    localStorage.removeItem('lastName');
    this.router.navigate(['/login']);
  }
}
